# dejavu

Code bookmark webapp powered by docker, firebase, vuejs, elasticsearch, golang nodejs etc ...

Allows a registered user to bookmark code snippets with some links (markdown and tags ...) and to search a déjà-vu code snippet for future usage/